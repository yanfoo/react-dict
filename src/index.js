import { useEffect, useMemo, useReducer } from 'react';

const defaultComparator = (a, b) => a === b;
const defaultTransform = v => v;

/**
 * 
 * @param {Object|Map|null} initialDict 
 * @param {Object} options 
 * @returns {ReactDict}
 */
const reactDict = (initialDict, options) => {
   const keyChanged = new Set();
   const subscribers = new Map();
   const comparator = options?.comparator ?? defaultComparator;
   const transform = options?.transform ?? defaultTransform;
   
   if (typeof comparator !== 'function') throw new TypeError('Invalid comparator for ReactVar');
   if (typeof transform !== 'function') throw new TypeError('Invalid transform for ReactVar');

   let dictionary = initialDict instanceof Map ? initialDict : new Map(Object.entries(initialDict || {}));

   // per props lock
   let keyLocks = new Set();
   let handlerLocks = new Set();

   const setValue = async newDict => {
      
      if (!(newDict instanceof Map)) {
         newDict = new Map(Object.entries(newDict || {}));
      }

      // check locks
      for (const key of newDict.keys()) {
         if (keyLocks.has(key)) throw new Error(`Cannot modify ReactDict[${key}] while it is already being modified!`);
      }
      // lock
      for (const key of newDict.keys()) {
         keyLocks.add(key);
      }

      try {

         for (let [ key, newValue ] of newDict) {
            const value = dictionary.get(key);
            if (newValue instanceof Function) {
               newValue = newValue(value);
            }
            const valueChanged = !comparator(newValue, value, key);
            newValue = await transform(newValue, value, valueChanged, key);
            dictionary.set(key, newValue);

            if (valueChanged) keyChanged.add(key);
         }

         if (keyChanged.size) {
            for (const [ handler, handlerKeys ] of subscribers) {
               // only notify on the keys subscribed to with this handler
               const updatedKeys = Array.from(handlerKeys).filter(key => keyChanged.has(key));
               if (updatedKeys.length && !handlerLocks.has(handler)) {
                  handlerLocks.add(handler);
                  try {
                     await handler({ updatedKeys, values: () => ReactDict.values(handlerKeys) });
                  } finally {
                     handlerLocks.delete(handler);
                  }
               }
            }
         }

         return ReactDict;
      } finally {
         for (const key of newDict.keys()) {
            keyLocks.delete(key);
         }
         keyChanged.clear();
      }
   };

   const ReactDict = newDict => setValue(newDict);

   Object.defineProperties(ReactDict, {
      dictionary: {
         configurable: false,
         enumerable: true,
         get: () => Object.freeze(Object.fromEntries(dictionary))
      },
      values: {
         configurable: false,
         enumerable: true,
         writable: false,
         value: keys => {
            const values = {};

            if (keys && keys[Symbol.iterator]) {
               if (!Array.isArray(keys)) {
                  keys = Array.from(keys);
               }

               for (const key of keys) {
                  values[key] = dictionary.get(key);
               }
            }

            return values;
         }
      },
      subscribe: {
         configurable: false,
         enumerable: false,
         writable: false,
         value: (handler, keys) => { 
            keys = keys instanceof Set ? keys : new Set(keys);

            if (!keys.size) throw new Error('Must subscribe to at least one key');

            subscribers.set(handler, keys);
            return () => subscribers.delete(handler);
         }
      },
      unsubscribe: {
         configurable: false,
         enumerable: false,
         writable: false,
         value: handler => subscribers.delete(handler)
      }
   });

   return Object.freeze(ReactDict);   
};


/**
 * @param {Object|Map|null} initialDict 
 * @param {Object} options 
 * @returns {ReactDict}
 */
const createReactDict = (initialDict, options) => reactDict(initialDict, options);



const reactDictState = ({ reactDict, keys }, { values }) => ({ values:values(), reactDict, keys });

/**
 * @param {ReactDict} reactdict 
 * @param {Iterable<String>} keys
 * @returns {Tuple<Array<Any>,Function>}
 */
const useReactDict = (reactDict, keys)  => {
   const keyStr = JSON.stringify(keys);
   const memoKeys = useMemo(() => JSON.parse(keyStr), [keyStr]);
   const [ { values }, handler ] = useReducer(reactDictState, useMemo(() => reactDictState({ reactDict, memoKeys }, { values: () => reactDict.values(memoKeys) }), [reactDict, memoKeys]));
   useEffect(() => reactDict.subscribe(handler, memoKeys), [memoKeys, reactDict]);
   return values;
};


//export default ReactVar;
export { createReactDict, useReactDict }