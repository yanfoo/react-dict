
declare type ReactDictChangeEvent<T> = {
   /**
    * The keys that have been updated
    */
   updatedKeys: Array<string>,
   /**
    * The current handled values (including the keys that haven't updated)
    */
   values(): Record<string,T>
}


export type ReactDict<T> = {
   /**
    * Set a new value and notify subscribers
    */
   (newDict?: Record<string,T> | ((dict: Record<string,T>) => Record<string,T>)): Promise<ReactDict<T>>;
   /**
    * Get a 
    */
   dictionary: Record<string,T>
   /**
    * Get the current values for the specified keys
    */
   values(keys: Iterable<string>): Record<string,T>;
   /**
    * Register a new subscriber. Returns a function to unsubsribe the handler
    * @param handler will be called with the updated value
    */
   subscribe(handler: (event: ReactDictChangeEvent<T>) => Promise<void> | void): () => void;
   /**
    * Unregister a subscriber
    * @param handler must be the exact same subscribed handler
    */
   unsubscribe(handler: (event: ReactDictChangeEvent<T>) => Promise<void> | void): void;
};

export type ReactDictOptions<T> = {
   comparator: (newVal: T, oldVal: T) => boolean,
   transform: (newVal: T, oldVal: T, changed: boolean, key: string) => Promise<T> | T
};

export function createReactDict<T>(initialValue: T, options:ReactDictOptions<T>): ReactDict<T>;

/**
 * Use the specified dictionary, subscribing to only the specified keys
 * @param reactDict 
 * @param keys the list of keys to subscribe reactively
 */
export function useReactDict<T>(reactDict: ReactDict<T>, keys: Iterable<string>): Record<string,T>