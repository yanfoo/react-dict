import React from 'react'
import { render, screen, act } from '@testing-library/react'

import { createReactDict, useReactDict } from '../src/index';


describe('Testing entry point', () => {

   it('should expose provider and hooks', () => {
      expect(createReactDict).toBeInstanceOf(Function);
      expect(useReactDict).toBeInstanceOf(Function);
   });


   describe('Testing createReactDict', () => {

      it('should create instance', () => {
         const d = createReactDict({ foo: true });

         expect(d).toBeInstanceOf(Function);
         expect(d.dictionary).toEqual({ foo: true });
         expect(d.values(['foo', 'bar'])).toEqual({ foo: true, bar: undefined });
      });

      it('should not allow modifying value directly', () => {
         const d = createReactDict({ foo: true });

         expect(() => d.dictionary.foo = false).toThrow('Cannot assign to read only property');
      });


      it('should notify subscriber', async () => {
         const d = createReactDict({ foo: true });
         let newFoo = true;

         d.subscribe(({ values }) => newFoo = values().foo, ['foo']);
         
         await d({ foo: false });

         expect(newFoo).toBe(false);

         await d(new Map(Object.entries({ foo: 'test' })));

         expect(newFoo).toBe('test');
      });

      it('should not notify subscriber when listening different keys', async () => {
         const d = createReactDict({ foo: true });
         let newFoo = true;

         d.subscribe(({ values }) => newFoo = values().foo, ['bar']);  // other property
         
         await d({ foo: false });

         expect(newFoo).toBe(true);
      });

      it('should not notify subscribers for unchanging value', async () => {
         const d = createReactDict({ foo: true });
         let notified = false;

         d.subscribe(() => notified = true, ['foo']);
         
         await d({ foo: true });

         expect(notified).toBe(false);
      });

      it('should not notify subscriber from empty new values', async () => {
         const d = createReactDict({ foo: true });

         d.subscribe(() => { throw new Error('Failed'); }, ['foo']);

         await expect(d()).resolves.not.toThrow();
         await expect(d(new Map())).resolves.not.toThrow();
      });

      it('should not recursively modify value', async () => {
         const d = createReactDict({ foo: true });

         d.subscribe(() => d({ foo: 'illegal' }), ['foo']);

         await expect(d({ foo: false })).rejects.toThrow('Cannot modify ReactDict[foo] while it is already being modified');
      });

      it('should recursively modify different values', async () => {
         const d = createReactDict({ foo: true, bar: true });
         let fooSub = 0;
         let barSub = 0;

         d.subscribe(() => {
            fooSub++;
            d({ bar: 'another' });
         }, ['foo']);
         d.subscribe(() => {
            barSub++;
         }, ['bar']);
         d.subscribe(() => {
            fooSub++;
            barSub++;
         }, ['foo', 'bar']);

         await d({ foo: false });

         expect(d.values(['foo', 'bar'])).toEqual({ foo:false, bar:'another' });
         expect(fooSub).toBe(2);
         expect(barSub).toBe(2);
      });

      it('should update using functions', async () => {
         const d = createReactDict({ foo: 'foo', bar: 'bar' });

         await d({ foo: v => v + '1', bar: v => v + '2' });

         expect(d.values(['foo', 'bar'])).toEqual({ foo:'foo1', bar:'bar2' });
      });

      it('should fail when no keys are subscribed', () => {
         const d = createReactDict({ foo: 'foo', bar: 'bar' });

         expect(() => d.subscribe(() => [])).toThrow('Must subscribe to at least one key');
         expect(() => d.subscribe(() => [], [])).toThrow('Must subscribe to at least one key');
         expect(() => d.subscribe(() => [], new Set())).toThrow('Must subscribe to at least one key');
      });

      it('should unsubscribe', async () => {
         const d = createReactDict({ foo: true });
         const handler = () => newFoo = false;
         let newFoo = true;

         d.subscribe(handler, ['foo']);  // other property
         d.unsubscribe(handler);
         
         await d({ foo: false });

         expect(newFoo).toBe(true);
         expect(d.dictionary.foo).toBe(false);
      });

   });


   describe('Testing options', () => {

      it('should throw if invalid comparator', () => {
         [
            false, true, 'bob', {}, [], 0, 1
         ].forEach(comparator => expect(() => createReactDict(null, { comparator })).toThrow('Invalid comparator'));
      });

      it('should throw if invalid transform', () => {
         [
            false, true, 'bob', {}, [], 0, 1
         ].forEach(transform => expect(() => createReactDict(null, { transform })).toThrow('Invalid transform'));
      });

      it('should default dictionary', () => {
         expect(new createReactDict().dictionary).toEqual({});
         expect(new createReactDict(undefined).dictionary).toEqual({});
         expect(new createReactDict(null).dictionary).toEqual({});
         expect(new createReactDict({}).dictionary).toEqual({});
         expect(new createReactDict(new Map()).dictionary).toEqual({});
      });

   });


   describe('Testing methods', () => {

      it('should return empty values', () => {
         const d = createReactDict();

         expect(d.values()).toEqual({});
         expect(d.values([])).toEqual({});
         expect(d.values(new Set())).toEqual({});
      })

   });


   describe('Testing useReactVar', () => {

      const TestContainer = ({ reactDict }) => {
         const values = useReactDict(reactDict, ['foo', 'bar']);

         return (
            <div data-testid="output">{ Object.entries(values).map(([,value]) => value).join(',') }</div>
         );
      };


      it('should output values', async () => {
         const d = createReactDict({ foo: 'hello', bar: 'world' });

         await act(async () => {
            render(<TestContainer reactDict={ d } />);
         });
   
         expect(screen.getByTestId('output')).toHaveTextContent('hello,world');
      });

      it('should update output value', async () => {
         const d = createReactDict({ foo: 'hello', bar: 'world' });

         act(() => {
            render(<TestContainer reactDict={ d } />);
         });
   
         await act(async () => {
            await d({ foo: 'Hello' });
         });

         expect(screen.getByTestId('output')).toHaveTextContent('Hello,world');
      });

   });


});